#!/bin/bash

EASY_RSA_DIR="/usr/share/easy-rsa"
EASY_RSA_VERSION="2.2.2"

rm -rf ${EASY_RSA_DIR}/*

mkdir -p $EASY_RSA_DIR/$EASY_RSA_VERSION
(
    cd "$EASY_RSA_DIR/$EASY_RSA_VERSION"
    if echo "$EASY_RSA_VERSION" | grep -q ^2; then
        wget "https://github.com/OpenVPN/easy-rsa/releases/download/${EASY_RSA_VERSION}/EasyRSA-${EASY_RSA_VERSION}.tgz"
    else
        wget "https://github.com/OpenVPN/easy-rsa/releases/download/v${EASY_RSA_VERSION}/EasyRSA-${EASY_RSA_VERSION}.tgz"
    fi
    tar xzvf "EasyRSA-${EASY_RSA_VERSION}.tgz"
    rm -f "EasyRSA-${EASY_RSA_VERSION}.tgz"
    mv EasyRSA-${EASY_RSA_VERSION}/* .
    rm -rf EasyRSA-${EASY_RSA_VERSION}
    cd "$EASY_RSA_DIR"
    ln -snf "${EASY_RSA_VERSION}" "$(echo ${EASY_RSA_VERSION%.*})"
    ln -snf "${EASY_RSA_VERSION}" "$(echo ${EASY_RSA_VERSION%%.*})"
)
